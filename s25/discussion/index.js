//  JSON Object
/*
	- JSON stands for Javascript Object Notation
	Syntax: 
	{
	"propertyA": "valueA",
	"propertyB": "valueB"
	}
*/


// JSON as Objects
/*{
	"city": "Quezon City",
	"province": "Metro Manila",
	"country": "Philippines"
}

// JSON Arrays
"cities": [
	{"city": "Quezon city", "province": "Metro Manila", "country": "Philippines"},
	{"city": "Manila city", "province": "Metro Manila", "country": "Philippines"},
	{"city": "Makati city", "province": "Metro Manila", "country": "Philippines"}
]*/

// Mini activity - Create a JSON Array the will hold three breeds of dogs with properties: name, age, breed.

/*"dogs": [
{"name": "James", "age": "2yrs old", "breed": "Aspin"},
{"name": "Edward", "age": "3yrs old", "breed": "Huspin"}
]*/

// JSON Methods

// Convert Data into Stringified JSON

let batchesArr = [{ batchName: 'Batch X'}, { batchName: 'Batch Y' }];

//  the "STRINGIFY" method is used to convert JS objects into a string
console.log('Result of Stringify method: ');
console.log(JSON.stringify(batchesArr));

let data = JSON.stringify({
	name: 'John',
	age: 31,
	address: {
		city: 'Manila',
		country: 'Philippines'
	}
});

console.log(data);

//  Using Stringify method with variables
//  User details
/*let firstName = prompt('What is your first name?');
let lastName = prompt('What is your last name?');
let age = prompt('What is your age?');
let address = { 
	city: prompt('Which city do you live?'),
	country: prompt('Which country does your city address belong to?')
}

let otherData = JSON.stringify({
	firstName: firstName,
	lastName: lastName,
	age: age,
	address: address

})

console.log(otherData);*/

// Mini Activity - create a JSON data that will accept user car details with variables brand, type , year.

/*let brand = prompt('What is your car brand?');
let type = prompt('What is your car type?');
let year = prompt('What is your car year model?')

let carData = JSON.stringify({
	brand: brand,
	type: type,
	year: year

})

console.log(carData);*/

// Converting stringified JSON into JS objects

let batchesJSON = '[{"batchName": "Batch X"}, {"batchName": "Batch Y"}]';

console.log('Result from parse method');
console.log(JSON.parse(batchesJSON));