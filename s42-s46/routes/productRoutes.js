// [SECTION] Dependencies
const express = require('express');
const router = express.Router();
const productController = require('../controllers/productController');
const auth = require('../auth');

// [SECTION] Routes
// Route for adding a product
router.post("/add", auth.verify, (req, res) => {
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin == true) {
		productController.addProduct(data)
			.then(resultFromController => res.send(resultFromController));
	} else {
		return false;
	}
})

// Route for adding multiple products
router.post("/addMany", auth.verify, (req, res) => {
	const data = {
		products: req.body.products,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin == true) {
		productController.addProducts(data)
			.then(resultFromController => res.send(resultFromController));
	} else {
		return false;
	}
})

// Route for retrieving all products
router.get("/all", (req, res) => {
	productController.getAllProducts()
		.then(resultFromController => res.send(resultFromController));
})

// Route for retrieving all ACTIVE products
router.get("/active", (req, res) => {
	productController.getAllActive()
		.then(resultFromController => res.send(resultFromController));
})

// Route for retrieving ON SALE PRODUCTS
router.get("/sale", (req, res) => {
	productController.getOnSale()
		.then(resultFromController => res.send(resultFromController));
})

// Route for retrieving products under the client's budget
router.get("/budget", (req, res) => {
  	const maxPrice = req.query.maxPrice;

  	productController.getOnBudget(maxPrice)
    	.then((resultFromController) => res.send(resultFromController));
});

// Route for retrieving a specific product based on product ID
router.get("/:productId", (req, res) => {
	console.log(req.params.productId);

	productController.getProduct(req.params)
		.then(resultFromController => res.send(resultFromController));
})

// Route for updating any detail
router.put('/:productId', auth.verify, (req, res) => {

	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin == true){
		console.log(req.params.productId);

		const productId = req.params.productId;
		const updateData = req.body;

		productController.updateProduct({productId}, updateData)
			.then(resultFromController => res.send(resultFromController));
		} else {
			return false;
		}

})

// Route for archiving a product
router.put("/:productId/archive", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin == true){
		productController.archiveProduct(req.params)
			.then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false);
	}
})

// [SECTION] Export
module.exports = router;