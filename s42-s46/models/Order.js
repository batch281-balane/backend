// [SECTION] Dependencies
const mongoose = require('mongoose');

// [SECTION] Schema declaration
const orderSchema = new mongoose.Schema({
	clientId: {
		type: String
	},
	products: [{
		productId: {
			type: String
		},
		sku: {
			type: String
		},
		quantity: {
			type: Number
		},
		subtotal: {
			type: String
		}
	}],
	total: {
		type: Number
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	},
	checkOut: {
		type: Boolean,
		default: false
	}
})

// [SECTION] Export
module.exports = mongoose.model("Order", orderSchema);