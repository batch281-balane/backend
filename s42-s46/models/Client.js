// [SECTION] Dependencies
const mongoose = require('mongoose');

// [SECTION] Schema declaration
const clientSchema = new mongoose.Schema({
	acctName: {
		type: String,
		required: [true, "First name is required."]
	},
	email: {
		type: String,
		required: [true, "Email is required."]

	},
	password: {
		type: String,
		required: [true, "Password is required."]
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	address:{ 
		houseNo: {
		    type: String,
		    required: [true, "House number is required."]
		},
		street: {
		    type: String
		},
		barangay: {
		    type: String,
		    required: [true, "Barangay is required."]
		},
		city: {
		    type: String,
		    required: [true, "City is required."]
		},
		province: {
		    type: String,
		    required: [true, "Province is required."]
		},
		country: {
			type: String,
			required: [true, "Country is required."]
		}
	},
	acctRep: {
		type: String,
		required: [true, "Account representative is required."]
	},
	mobileNo: {
		type: String,
		required: [true, "Mobile number is required."]
	},
	acctMgr: [{
		adminId: {
			type: String
		},
		firstName: {
			type: String
		},
		lastName: {
			type: String
		}
	}],
	carts: [{
		cartId: {
			type: String
		},
		products: [{
			productId: {
				type: String
			},
			sku: {
				type: String
			},
			quantity: {
				type: Number
			},
			subtotal: {
				type: String
			}
		}],
		total: {
			type: Number
		},
		purchasedOn: {
			type: Date,
		},
		checkOut: {
			type: Boolean,
		}
	}],
	isActive: {
		type: Boolean,
		default: true
	},
	isAdmin: {
		type: Boolean,
		default: false
	}

})

// [SECTION] Export
module.exports = mongoose.model("Client", clientSchema);