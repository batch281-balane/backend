//  Using require directive to load http module
let http = require("http");

// Questions

/*{
	// 1. What directive is used by node.js in loading the modules it needs?
	response.writeHead(200, {'Content-Type': 'text/plain'});
	// Answer:
	Require directive
	
}
{
	// 2. What Node.js module contains a method for server creation?
	response.writeHead(200, {'Content-Type': 'text/plain'});
	// Answer:
	 http module
	
}
{
	// 3. What is the method of the http object responsible for creating a node server using Node.js?
	response.writeHead(200, {'Content-Type': 'text/plain'});
	// Answer:
	 http. createServer()
	
}
{
	// 4. What method of the response object allows us to set status codes and content types?
	response.writeHead(200, {'Content-Type': 'text/plain'});
	// Answer:
	Setting Status Codes
	
}
{
	// 5. Where will console.log() output its  contents when run in node.js?
	response.writeHead(200, {'Content-Type': 'text/plain'});
	// Answer:
	 the web console
	
}
{
	// 6. What property of the request object contains the address's endpoint?
	response.writeHead(200, {'Content-Type': 'text/plain'});
	// Answer:
	response.end
	
}*/

// Create a server
http.createServer(function (request, response) {
	// Returning what type of response being thrown to the client
	response.writeHead(200, {'Content-Type': 'text/plain'});
	// Send the response with the text content 'Hello, World!'
	response.end('Hello, Activity!');
}).listen(3000);

// When server is running, console will print the message:
console.log('Server is running at localhost:3000')