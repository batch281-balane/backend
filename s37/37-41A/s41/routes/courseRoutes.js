const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseController");
const auth = require("../auth");


// [ACTIVITY] Route for  creating a course
// My solution
// router.post("/", (req, res) => {
// 	let userData = auth.decode(req.headers.authorization);

// 	if(userData.isAdmin){
// 		courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController));
// 	} else {
// 		res.status(403).send("Not an admin.");
// 	}
// })

// [ACTIVITY SOLUTION]
router.post("/", auth.verify, (req, res) => {

	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin == true) {
		courseController.addCourse(data).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false);
	}

})

// Route for retrieving all the course
router.get("/all", (req, res) => {
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController));
})

// Route for retrieving all courses w/ active status
router.get("/", (req, res) => {
	courseController.getAllActive().then(resultFromController => res.send(resultFromController));
})

// Route for retrieving a specific course
// Creating a course using "/:parametername" creates a dynamic route, meaning the url changes depending on the information provided
router.get("/:courseId", (req, res) => {
	console.log(req.params.courseId);

	// Since the course ID will be sent via the URL, it can not retrieve it from the request body
	// We can however retrieve the course ID by accessing the request's "params" property which contains all the parameters provided via the URL
	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
})

// Route for updating a course
// auth.verify here is used just to check if user is logged in
router.put("/:courseId", auth.verify, (req, res) => {
	// req.params is to check the id
	// req.body is to update the body
	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
})

// [ACTIVITY] Archiving a COURSE
// Route for archiving a course
// My Solution
/*router.patch("/:courseId/archive", (req, res) => {
	let userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin) {
		courseController.archiveCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
	} else {
		res.send("Not an admin");
	}
});*/

// [ACTIVITY SOLUTION] s40
// Route to archiving a course
// A "PUT" request is used instead of "DELETE" request because of our approach in archiving and hiding the courses from our users by "soft deleting" records instead of "hard deleting" records which removes them permanently from our databases
router.put("/:courseId/archive", auth.verify, (req, res) => {

	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin == true){
		courseController.archiveCourse(req.params).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false);
	}
})

// Allows us to export the "router" object that will be accessed in our index.js file
module.exports = router;