const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.checkEmailExists = (reqBody) => {
	return User.find({email : reqBody.email}).then(result => {
		
		// The find method returns a record if a match is found
		if(result.length > 0){
			return true;
		} 
		// result.length = 0
		// No duplicate emails found
		// The user is not registered in the database
		else {
			return false;
		}
	})
}

// User Registration
module.exports.registerUser = (reqBody) => {
	
	// Creates a variable named "newUser" and instantiates a new "User" object using the Mongoose model
	let newUser = new User ({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		mobileNo : reqBody.mobileNo,
		password : bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) => {

		// User registration fail
		if(error){
			return false;
		} 

		// User registration
		else {
			return true;
		}
	})
}

// User authentication
module.exports.loginUser = (reqBody) => {
	
	// Returns the first record in the collection that matches the search criteria
	// findOne method instead of find method which returns all records that match the criteria
	return User.findOne({email : reqBody.email}).then(result => {

		// User does not exist
		if(result  == null) {
			return false;
		}
		else {
			// Creates the variable "isPasswordCorrect" to return the result of comparing the login form password and the database password
			// The "compareSync" method is used to compare a non encrypted password from the login form to the encrypted password retrieved from the database and returns "true" or "false" value depending on the result
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			// If the password matches
			if(isPasswordCorrect) {
				// Generate an access token
				// Uses the createAccessToken method defined in the auth.js file
				return { access: auth.createAccessToken(result) };
			}

			// Passwords do not match
			else {
				return false;
			}
		}
	})
}

// User detail retrieval
module.exports.getProfile = (data) => {

	return User.findById(data.userId).then(result => {

		// Changes the value of the user's password to an empty string when returned to the frontend
		// Not doing so will expose the user's password which will also not be needed in other parts of our application
		// Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
		result.password = "";

		// Returns the user information with the password as an empty string
		return result;

	});

};
