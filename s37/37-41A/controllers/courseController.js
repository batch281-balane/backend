const Course = require("../models/Course");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.addCourse = (data) => {

	// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
	// Uses the information from the request body to provide all the necessary information
	let newCourse = new Course ({
		name: data.course.name,
		description: data.course.description,
		price: data.course.price
	});

	return newCourse.save().then((course, error) => {
		if(error){
			return false;
		} else {
			return true;
		}
	})
}

// Retrieving all course
module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	})
}

// Retrieving all active courses
module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then(result => {
		return result;
	})
}

// Retrieving a specific course
module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		return result
	});
};

// Updating a specific course
module.exports.updateCourse = (reqParams, reqBody) => {

	// Specify the fields/ properties of the document to be updated
	let updatedCourse = {
		name : reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};

	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {

		// Course not updated
		if(error){
			return false;
		}
		// Course updated successfully
		else {
			return true;
		}

	})
}

// [ACTIVITY] Archiving a specific course
module.exports.archiveCourse = (reqParams, reqBody) => {

	let archivedCourse = {
		isActive: reqBody.isActive
	};

	return Course.findByIdAndUpdate(reqParams.courseId, archivedCourse).then((course, error) => {

		if(error){
			return false;
		} else {
			return true
		}
	})
}