const express = require('express');
const router = express.Router();
const userController = require("../controllers/userControllers");
const auth = require("../auth");

// Route for checking if the email already exists in the database
// Invokes the checkEmailExists function from the controller file
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for user registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));	
})

// Route for retriving user details
// The "auth.verify" acts a middleware to ensure that the user is logged in before they can enroll to a course
router.post("/details", (req, res) => {

	// Uses decode method defined in the auth.js file to retrieve the user information
	const userData = auth.decode(req.headers.authorization);

	// Provides the user's ID for the getProfile controller method
	userController.getProfile({userId: userData.id}).then(resultFromController => res.send(resultFromController));
	
	// INSTRUCTION ON POSTMAN: Login first, get the token
		// Then create a new POSTMAN request
		// GET. Authorization. Paste the token on Bearer token
})

module.exports = router;