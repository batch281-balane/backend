const express = require("express");
const mongoose = require("mongoose");

const app = express();

// Connect to our MongoDB
mongoose.connect("mongodb+srv://jayarbalane1313:Oxfkb5aoV3AN6MLW@wdc028-course-booking.yb9zcao.mongodb.net/?retryWrites=true&w=majority",
{
	// Deprecators: It will give warnings if there are concerns
	useNewUrlParser : true,
	useUnifiedTopology : true
});

let db = mongoose.connection;
db.on('error', console.error.bind(console, "MongoDB Connection Error."));
db.once('open',() => console.log('Now connected to MongoDB Atlas!'));

app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${process.env.PORT|| 4000}` )
})