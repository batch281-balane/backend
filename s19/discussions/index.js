// [SECTION] Selection Control Structures
/*

	- it sorts out whether the statements are to be executed based on the condition whether it is true or false.
	- it can be two-way (true or false)
	- it can also be multi-way selection

*/

//  If .. else statement



/*

syntax: if(condition){
	// statement
} else {
	//statement
}

*/

//  IF statement - executes a statement if a specified condition is true. 


let numA = -1;
if(numA < 0) {
	console.log("The number is less than 0");
}
console.log(numA < 0);

//  string sample

let city = "New York";
if(city === "New York"){
	console.log("Welcome to New York City!");
}


// another sample for else if

/*
	- executes a statement if the previous conditions are false and th if specified condition is true.
	- the "else if" clause is optional and can be added to capture additionals conditions to change the flow of the programs.
*/

let numB = 1;

if(numA < 0) {
	console.log("Hello");
} else if (numB > 0) {
	console.log("World");
}

city = "Tokyo";
if(city === "New York") {
	console.log("Welcome to New York City!");
} else if (city === "Tokyo") {
	console.log("Welcome to Tokyo"); 
}

// Else 
	//  -  executes a statement if all of our previous conditions are false. 

if(numA > 0) {
	console.log("Hello");
} else if(numB === 0) {
	console.log("Hello World");
} else {
	console.log("Again");
}

// let age = parseInt(prompt("Enter your age: "));
// if (age <= 18) {
// 	console.log("Not Allowed to drink");
// } else {
// 	console.log("Matanda ka na! Shot Puno!");
// }

/*
	Mini Activty
		-create a function that will receive any value of height as an argument when you invoke it.
		-then create conditional statements:
			- if height is less than or equal to 150, print "Did not pass the min height requirement." in the console.
			- but if the height is greater than 150, print "Passed the min height requirement." in the console
*/

/* let height = parseInt(prompt("Enter your height: "));
if (height <= 150) {
	console.log("Didn't pass the min height req");
} else {
	console.log("Passed the min height req");
} */


//  main purpose of function is for reusability
function minHeight(height) {
	if (height <= 150) {
		console.log("Didn't pass the min height req.");
	} else if (height > 150){
		console.log("Passed the min height req.");
	}
}
minHeight(149);
minHeight(160);

let message = "No Message";
console.log(message);

function determinTyphoonIntensity(windSpeed){
	if(windSpeed < 30) {
		return 'Not a typhoon'
	} else if (windSpeed <= 61){
		return 'Tropical depression detected'
	} else if (windSpeed >= 62 && windSpeed <= 88) { return 'Tropical storm detected'
	} else if (windSpeed >= 89 && windSpeed <= 177) {
		return 'Severe tropical storm detected'
	} else {
		return'Typhoon Detected'
	}
}

message = determinTyphoonIntensity(70);
console.log(message);

if(message == "Tropical storm detected") {
	console.warn(message);
}

// Truthy and Falsy

/*
	In JS a truthy value is a value that is considered true when encountered in a boolean context.

	Falsy value:
		1. false
		2. 0
		3. -0
		4. ""
		5. null
		6. undefined
		7. NaN
*/	

// Truthy Examples:

let word = "true"
if(word){
	console.log("Truthy");
};

if(true) {
	console.log("Truthy");
};

if(1) {
	console.log("Truthy");
};


// Falsy Examples:
if(false) {
	console.log("Falsy");
};

if(0) {
	console.log("Falsy");
};

if(undefined) {
	console.log("Falsy");
};

if(null) {
	console.log("Falsy");
};

if(-0) {
	console.log("Falsy");
};

if(NaN) {
	console.log("Falsy");
};

// conditional (ternary) operator - for short codes

/*

	Ternary operator takes in three operands
	1. condition
	2. expression to execute if the condition is true/truthy.
	2. expression to execute if the condition is false/falsy.

	Syntax: 
	 (condition) ? ifTrue_expression : isFalse_expression.

*/

// Single statement execution

let ternaryResult = (1 < 18) ? "Condition is true" : "Condition is False";

console.log("result of the ternary operator: " + ternaryResult);

// Multiple Statement Execution
let name;

function isOfLegalAge() {
	name = "John";
	return "You're of the legal age limit";
}

function isUnderAge() {
	name = "Jane";
	return "You're under the age limit";
}

// let yourAge = parseInt(prompt("What is your age?"));
// console.log(yourAge);

// let legalAge = (yourAge > 18) ? isOfLegalAge() : isUnderAge();
// console.log("Result of the ternary operator in function: " + legalAge + ", " + name);


// [Section] switch statement

/*

	Can be used as an alternative to if ... else 
	statements where the data to be used in the condition is of an expected input. 

	syntax: 
		switch (expression/condition) {
			case <value>;
				statement;
				break;
			default:
				statement;
				break;
		}

*/

let day = prompt("What day of the week is it today?").toLowerCase();

console.log(day);

switch (day) {
case 'monday':
	console.log("The color of the day is red.");
	break;
case 'tuesday':
	console.log("The color of the day is orange.");
	break;
case 'wednesday':
	console.log("The color of the day is yellow.");
	break;
case 'thursday':
	console.log("The color of the day is green.");
	break;
case 'friday':
	console.log("The color of the day is blue.");
	break;
case 'saturday':
	console.log("The color of the day is indigo.");
	break;
case 'sunday':
	console.log("The color of the day is violet.");
	break;
default:
	console.log("Please Input a valid day of the week.");
	break;

}

// break; - is for ending loop.
// switch - is not a very popular statement in coding.

// [Section] Try-Catch-Finally Statement

/*
 - try catch is commonly used for error handling
 - will still function even if the statment is not complete/without the finally code block.

*/

function showIntensityAlert(windSpeed) {
	try{
		alert(determinTyphoonIntensity(windSpeed))
	}
	catch (error) {
		console.log(typeof error)
		console.log(error)
		console.warn(error.message)
	}
	finally {
		alert("Intensity updates will show new alert!")
	}
}

showIntensityAlert(56);

let error = 0;
console.log(error)    // error cann be used as variable name. 