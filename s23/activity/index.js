// console.log("Hello World");

//Strictly Follow the property names and spelling given in the google slide instructions.
//Note: Do not change any variable and function names. 
//All variables and functions to be checked are listed in the exports.

function Trainer(Name, Age, Pokemon, Friends){
    this.Name = Name;
    this.Age = Age;
    this.Pokemon = Pokemon;
    this.Friends = Friends['Max' , 'May']
}

// let trainer = new Trainer('Ash Ketchum', 10, ['Bulbasaur', 'Charmander', 'Totodile', 'Bayleef'], 'Hoen:', );
// console.log(trainer);

// Create an object called trainer using object literals

// Initialize/add the given object properties and methods

let trainer = {
    Name: 'Ash Ketchum',
    Age: 10,
    Pokemon: ['Bulbasaur', 'Charmander', 'Totodile', 'Bayleef'],
    Friends: {Hoenn: ['Max', 'May'], Kanto: ['Brock', 'Misty'] },
    talk: function(){
        console.log("Bayleef! I choose you!")
    }
}
console.log(trainer);

// Properties

// Methods

// Check if all properties and methods were properly added


// Access object properties using dot notation
console.log('Result of dot notation:');
console.log(trainer.Name);

// Access object properties using square bracket notation
let array = [trainer]
console.log('Result of square bracket notation: ');
console.log(array[0]['Pokemon']);
// Access the trainer "talk" method
trainer.talk();
// Create a constructor function called Pokemon for creating a pokemon


// Create/instantiate a new pokemon
let myPokemon = {
    name: "Totodile",
    level: 5,
    health: 100,
    attack: 50,
    tackle: function() {
        console.log("This Pokemon tackled targetPokemon");
        console.log("targetPokemon's health is now reduced to _targetPokemonhealth_");
    },
    faint: function() {
        console.log("Pokemon fainted");
    }
}
console.log(myPokemon);


// Create/instantiate a new pokemon
let wildPokemon = {
    name: "Pigeon",
    level: 3,
    health: 100,
    attack: 50,
    tackle: function() {
        console.log("This Pokemon tackled targetPokemon");
        console.log("targetPokemon's health is now reduced to _targetPokemonhealth_");
    },
    faint: function() {
        console.log("Pokemon fainted");
    }
}
console.log(wildPokemon);

// function pokemon(name, level){
//     // Properties
//     this.name = name;
//     this.level = level;
//     this.health = 2 * level;
//     this.attack = level;
//     // this.targetPokemonhealth = health - attack;

//     // Methods
//     this.tackle = function(target){
//         console.log(this.name + ' tackled '+ target.name);
//         console.log("targetPokemon's health is now reduced to _targetPokemonhealth_");
//     }
//     this.faint = function(){
//         console.log(this.name + ' fainted.');
//     }
// }
// console.log(pokemon)


// Create/instantiate a new pokemon
let myPokemon2 = {
    name: "Bayleef",
    level: 30,
    health: 100,
    attack: 50,
    tackle: function() {
        console.log("This Pokemon tackled targetPokemon");
        console.log("targetPokemon's health is now reduced to _targetPokemonhealth_");
    },
    faint: function() {
        console.log("Pokemon fainted");
    }
}
console.log(myPokemon2);


function Pokemon(name, level){
    // Properties
    this.name = name;
    this.level = level;
    this.health = Math.round(7.1 * level);
    this.attack = Math.round(3.4 * level);
    // this.targetPokemonhealth = health - attack;

    // Methods
    this.tackle = function(target){
        console.log(this.name +  " tackled " + target.name);
        target.health = target.health - this.attack;    
        console.log( target.name + "'s health is now reduced to " + target.health);
    
    if(target.health <=0){
        console.log(target.name + " fainted!");
    }
    }
}
console.log(Pokemon)

// Invoke the tackle method and target a different object

let Totodile = new Pokemon("Totodile", 5);
let Pigeon = new Pokemon("Pigeon", 3);
let Bayleef = new Pokemon("Bayleef", 30);
// Invoke the tackle method and target a different object
Totodile.tackle(Pigeon);

Pigeon.tackle(Totodile);

Bayleef.tackle(Pigeon);
console.log(Pigeon)





//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        trainer: typeof trainer !== 'undefined' ? trainer : null,
        Pokemon: typeof Pokemon !== 'undefined' ? Pokemon : null

    }
} catch(err){

}