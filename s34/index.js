// Use the "require" directive to load the express module/package
// A "module" is a software component or part of a program that contains one or more routines
// This is used to get the contents of the express package to be used by our application
// It also allows us to access methods and functions that will allow us to easily create a server
const express = require("express");

// Create an application using express
// This Creates an express application and stored this in a constant called app
//  In layman's terms, app is our server
const app = express();

// For our application server to run, we need a port to listen to
const port = 4000;

// Set up for allowing the server to handle data from requests
// Allows your app to read JSON data
// Methods used from express JS are middlewares
// Middleware is software that provides common services and capabilities to applications outside of what's offered  by the operating system
// API management is one of the common app of middlewares
app.use(express.json());

// Allows your app to read data from forms
// By Default, information received from the url can be received as a string or an array
// By applying the option of "extended:true" this allows us to receive information in other data types such as an object which we will use throughout our application
app.use(express.urlencoded({extended:true}));

// [Section] Routes
// Express has methods correspondiong to each HTTP method
// This route expects to receive a GET request at the base URI of "/"
// The full base URI for our local application for this route will be at "http://localhost:4000"
// This route will return a simple message back to the client
app.get("/", (req, res) => {
	// Once the route is accessed it will send a string response containing "Hellow World"
	// Compared to our previous session, res.send uses the node JS modules method
	// res.send uses the express JS module's method instead to send a response back to the client
	res.send("Hello World");
})


// This route expects 
app.get("/hello", (req, res) => {
	// Once the route is accessed it will send a string response containing "Hellow World"
	// Compared to our previous session, res.send uses the node JS modules method
	// res.send uses the express JS module's method instead to send a response back to the client
	res.send("Hello from the /hello endpoinnt");
})

// Tells our server to listern to the port
// If the port is accessed, we can run the server
// Returns a meesage to confirm that the server is running in the terminal

// if(require.main) would allow us to listen to the app directly if it is not imported to another module, it will run the app directly
// else, if it is needed to be imported, it will not run the app and instead export it to be used in another file
if(require.main === module){
	app.listen(port, () => console.log(`Server is running at port ${port}`))
}

module.exports = app;