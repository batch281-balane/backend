const express = require("express");

const app = express();
const port = 3000;

app.use(express.json());

const users = [
  {
    username: "luffy",
    password: "password",
  },
  {
    username: "mark",
    password: "password123",
  },
];

app.get("/home", (req, res) => {
  res.send("Welcome to home page");
});

app.get("/users", (req, res) => {
  res.send(users);
});

app.delete("/delete-user", (req, res) => {
  let message = "";
  if (users.length !== 0) {
    for (let i = 0; i < users.length; i++) {
      if (req.body.username === users[i].username) {
        message = `User ${req.body.username} is has been deleted!`;
        users.splice(i, 1);
        break;
      } else {
        req.body.username === ""
          ? (message = "Please input username.")
          : (message = "Username not found!");
      }
    }
    res.send(message);
  } else res.send("There are no users registered yet!");
});

app.listen(port, () => console.log(`Server running at port ${port}`));