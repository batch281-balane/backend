// Function with parameters

function printName(name) {
	console.log("My name is " + name);
}

printName("Juana");
printName("John");

let	sampleVariable = "Bella";
printName(sampleVariable);

function checkDivibilityBy8(num){
	let remainder = num % 8;
	console.log("The remainder of " + num + " divided by 8 is: " + remainder);
	let isDivisibleBy8 = remainder === 0;
	console.log("is " + num + " divisible by 8?");
	console.log(isDivisibleBy8);

}

checkDivibilityBy8(64);

// Functions as Arguments
function argumentFunction(){
	console.log("This function was passed as an argument before the message was printed.");
}
function invokeFunction(argumentFunction){
argumentFunction();
}

invokeFunction(argumentFunction);

// Using Multiple parameters
function createFullName(firstName, middleName, lastName){
	console.log(firstName + ' ' + middleName + ' ' + lastName);
}

createFullName('Juan', 'Perez', 'Dela Cruz');

// Using variables as arguments
let firstName = "John";
let middleName = "Doe";
let lastName = "Smith";

createFullName(firstName, middleName, lastName);

// Return Statement - allows us to output a value from a function to be passed to the line/block of code that invoked the function 

function returnFullName(firstName, middleName, lastName){
	return firstName + ' ' + middleName + ' ' + lastName;
	console.log("This message will not be printed.");
}

let completeName = returnFullName("Jeffrey", "Smith", "Bezos");
console.log(completeName);
console.log(returnFullName(firstName, middleName, lastName));

function returnAddress(city, country){
	let fullAddress = city + ', ' + country;
	return fullAddress;
}

let myAddress = returnAddress("Cebu city", "Philippines");
console.log(myAddress);